<?php

$connect = new PDO("mysql:host=localhost;dbname=uas_mobile", "root", "");
$received_data = json_decode(file_get_contents("php://input"));
$data = array();

// fungsi untuk menampilkan data 
if ($received_data->action == 'fetchall') {

    $query = "SELECT * FROM transkrip ORDER BY id_nomor DESC";
    $statement = $connect->prepare($query);
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
        $data[] = $row;
    }
    echo json_encode($data);
}

// fungsi untuk melakukan insert data
if ($received_data->action == 'insert') {

    $data = array(
        ':nim_mhs' => $received_data->nim,
        ':nama_mhs' => $received_data->nama_lengkap,
        ':grade_smt' => $received_data->grade_smt
    );

    $query = " INSERT INTO transkrip (nim_mhs, nama_mhs, grade_smt) VALUES (:nim_mhs, :nama_mhs, :grade_smt) ";

    $statement = $connect->prepare($query);

    $statement->execute($data);

    $output = array(
        'message' => 'Data Inserted'
    );

    echo json_encode($output);
}

// fungsi untuk menampilkan data yang akan di update
if ($received_data->action == 'fetchSingle') {

    $query = "SELECT * FROM transkrip WHERE id_nomor = '" . $received_data->id_nomor . "'";

    $statement = $connect->prepare($query);

    $statement->execute();

    $result = $statement->fetchAll();

    foreach ($result as $row) {
        $data['id_nomor'] = $row['id_nomor'];
        $data['nim_mhs'] = $row['nim_mhs'];
        $data['nama_mhs'] = $row['nama_mhs'];
        $data['grade_smt'] = $row['grade_smt'];
    }

    echo json_encode($data);
}

// fungsi untuk melakukan update data 
if ($received_data->action == 'update') {

    $data = array(
        ':nim_mhs' => $received_data->nim_mhs,
        ':nama_mhs' => $received_data->nama_mhs,
        ':grade_smt' => $received_data->grade_smt,
        ':id_nomor'   => $received_data->id_nomor
    );

    $query = "UPDATE transkrip SET nim_mhs = :nim_mhs, nama_mhs = :nama_mhs, grade_smt = :grade_smt WHERE id_nomor = :id_nomor";

    $statement = $connect->prepare($query);

    $statement->execute($data);

    $output = array(
        'message' => 'Data Updated'
    );

    echo json_encode($output);
}

// fungsi untuk melakukan delete data 
if ($received_data->action == 'delete') {

    $query = "DELETE FROM transkrip WHERE id_nomor = '" . $received_data->id_nomor . "'";

    $statement = $connect->prepare($query);

    $statement->execute();

    $output = array(
        'message' => 'Data Deleted'
    );

    echo json_encode($output);
}