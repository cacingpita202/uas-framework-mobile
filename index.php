<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>.:: Data Nilai ::.</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <style>
    .modal-mask {
        position: fixed;
        z-index: 9998;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .5);
        display: table;
        transition: opacity .3s ease;
    }

    .modal-wrapper {
        display: table-cell;
        vertical-align: middle;
    }

    .badge {
        padding: 1px 9px 2px;
        font-size: 12.025px;
        font-weight: bold;
        white-space: nowrap;
        color: #ffffff;
        background-color: #70b9c5;
        -webkit-border-radius: 9px;
        -moz-border-radius: 9px;
        border-radius: 9px;
    }

    .badge:hover {
        color: #ffffff;
        text-decoration: none;
        cursor: pointer;
    }

    .badge-error {
        background-color: #b94a48;
    }

    .badge-error:hover {
        background-color: #953b39;
    }

    .badge-warning {
        background-color: #f89406;
    }

    .badge-warning:hover {
        background-color: #c67605;
    }

    .badge-success {
        background-color: ##5cc45e;
    }

    .badge-success:hover {
        background-color: #356635;
    }

    .badge-info {
        background-color: #3a87ad;
    }

    .badge-info:hover {
        background-color: #2d6987;
    }

    .badge-inverse {
        background-color: #333333;
    }

    .badge-inverse:hover {
        background-color: #1a1a1a;
    }
    </style>
</head>

<body>
    <div class="container" id="crudApp">
        <br />
        <h3 align="center">Input Grade Nilai Mahasiswa</h3>
        <br />
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="panel-title">Mahasiswa</h3>
                    </div>
                    <div class="col-md-6" align="right">
                        <input type="button" class="btn btn-success btn-xs" @click="openModel" value="Add" />
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>NIM</th>
                            <th>Nama Mahasiswa</th>
                            <th>Grade Nilai</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        <tr v-for="row in allData">
                            <td>{{ row.nim_mhs }}</td>
                            <td>{{ row.nama_mhs }}</td>
                            <td>{{ row.grade_smt }}</td>
                            <td><button type="button" name="edit" class="btn btn-primary btn-xs edit"
                                    @click="fetchData(row.id_nomor)">Edit</button></td>
                            <td><button type="button" name="delete" class="btn btn-danger btn-xs delete"
                                    @click="deleteData(row.id_nomor)">Delete</button></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div v-if="myModel">
            <transition name="model">
                <div class="modal-mask">
                    <div class="modal-wrapper">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" @click="myModel=false"><span
                                            aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">{{ dynamicTitle }}</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>NIM</label>
                                        <input type="text" class="form-control" v-model="nim_mhs" />
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Mahasiswa</label>
                                        <input type="text" class="form-control" v-model="nama_mhs" />
                                    </div>
                                    <div class="form-group">
                                        <label>Grade Nilai</label>
                                        <input type="text" class="form-control" v-model="grade_smt" />
                                    </div>
                                    <br />
                                    <div align="center">
                                        <input type="hidden" v-model="id_nomor" />
                                        <input type="button" class="btn btn-success btn-xs" v-model="actionButton"
                                            @click="submitData" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </transition>
        </div>
    </div>
</body>

</html>

<script>
var application = new Vue({
    el: '#crudApp',
    data: {
        allData: '',
        myModel: false,
        actionButton: 'Insert',
        dynamicTitle: 'Add Data',
    },
    methods: {
        fetchAllData: function() {
            axios.post('api/action.php', {
                action: 'fetchall'
            }).then(function(response) {
                console.log(response.data);
                application.allData = response.data;
            });
        },
        openModel: function() {
            application.nim_mhs = '';
            application.nama_mhs = '';
            application.grade_smt = '';
            application.actionButton = "Insert";
            application.dynamicTitle = "Add Data";
            application.myModel = true;
        },
        submitData: function() {
            if (application.nim_mhs != '' && application.nama_mhs != '' && application.grade_smt != '') {
                if (application.actionButton == 'Insert') {
                    axios.post('api/action.php', {
                        action: 'insert',
                        nim_mhs: application.nim_mhs,
                        nama_mhs: application.nama_mhs,
                        grade_smt: application.grade_smt
                    }).then(function(response) {
                        application.myModel = false;
                        application.fetchAllData();
                        application.nim_mhs = '';
                        application.nama_mhs = '';
                        application.grade_smt = '';
                        alert(response.data.message);
                    });
                }
                if (application.actionButton == 'Update') {
                    axios.post('api/action.php', {
                        action: 'update',
                        nim_mhs: application.nim_mhs,
                        nama_mhs: application.nama_mhs,
                        grade_smt: application.grade_smt,
                        id_nomor: application.id_nomor
                    }).then(function(response) {
                        application.myModel = false;
                        application.fetchAllData();
                        application.nim_mhs = '';
                        application.nama_mhs = '';
                        application.grade_smt = '';
                        application.id_nomor = '';
                        alert(response.data.message);
                    });
                }
            } else {
                alert("Fill All Field");
            }
        },
        fetchData: function(id_nomor) {
            axios.post('api/action.php', {
                action: 'fetchSingle',
                id_nomor: id_nomor
            }).then(function(response) {
                application.nim_mhs = response.data.nim_mhs;
                application.nama_mhs = response.data.nama_mhs;
                application.grade_smt = response.data.grade_smt;
                application.id_nomor = response.data.id_nomor;
                application.myModel = true;
                application.actionButton = 'Update';
                application.dynamicTitle = 'Edit Data';
            });
        },
        deleteData: function(id_nomor) {
            if (confirm("Are you sure you want to remove this data?")) {
                axios.post('api/action.php', {
                    action: 'delete',
                    id_nomor: id_nomor
                }).then(function(response) {
                    application.fetchAllData();
                    alert(response.data.message);
                });
            }
        }
    },
    created: function() {
        this.fetchAllData();
    }
});
</script>